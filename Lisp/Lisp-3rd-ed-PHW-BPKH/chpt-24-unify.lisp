(defun elements-p (p d)
  (and (atom p) (atom d)))

(defun variable-p (p)
  (and (list p) (eq '? (first p))))

(defun recursive-p (p d)
  (and (list p) (list d)))

(defun unify (p1 p2 &optional bindings)
  (format t "unify: ~a expression : ~a, bindings: ~a~%"
	  p1 p2 bindings)
  (cond
    ((elements-p p1 p2)
     (unify-atoms p1 p2 bindings))
    ((variable-p p1)
     (unify-variable p1 p2 bindings))
    ((variable-p p2)
     (unify-variable p2 p1 bindings))
    ((recursive-p p1 p2)
     (unify-pieces p1 p2 bindings))
    (t 'fail)))

(defun unify-atoms (p1 p2 bindings)
  (if (eql p1 p2)
      bindings
      'fail))

(defun unify-pieces (p1 p2 bindings)
  (format t "unify-pieces: ~a expression : ~a, bindings: ~a~%"
	  p1 p2 bindings)
  (let ((result (unify (first p1) (first p2) bindings)))
    (if (eq 'fail result)
	'fail
	(unify (rest p1) (rest p2) result))))

(defun unify-variable (p1 p2 bindings)
  (format t "unify-variable variable: ~a expression : ~a, bindings: ~a~%"
	  p1 p2 bindings)
  (let ((binding (find-binding p1 bindings)))
    (if binding
	(unify (extract-value binding) p2 bindings)
	(if (insidep p1 p2 bindings)
	    'fail
	    (add-binding p1 p2 bindings)))))

(defun insidep (variable expression bindings)
  (format t "insidep variable: ~a expression : ~a, bindings: ~a~%"
	  variable expression bindings)
  (if (equal variable expression)
      nil
      (inside-or-equal-p variable expression bindings)))

(defun inside-or-equal-p (variable expression bindings)
  (format t "inside-or-equal-p variable: ~a expression : ~a, bindings: ~a~%"
	  variable expression bindings)
  (cond ((equal variable expression)
	 (format t "inside-or-equal-p returning true")
	 t)
	((atom expression) nil)
	((eq '? (first expression))
	 (let ((binding (find-binding expression bindings)))
	   (format t "inside-or-equal-p  binding: ~a~%" binding)
	   (when binding
	     (inside-or-equal-p variable
				(extract-value binding)
				bindings))))
	(t
	 (format t "first expression looks like this: ~a~%"
		 (first expression))
	 (format t "rest expression looks like this: ~a~%"
		 (rest expression))
	 (or (inside-or-equal-p variable
				  (first expression)
				  bindings)
	       (inside-or-equal-p variable
				  (rest expression)
				  bindings)))))
	
