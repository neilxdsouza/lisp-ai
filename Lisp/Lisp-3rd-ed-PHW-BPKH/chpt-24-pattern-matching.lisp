(defun add-binding (pattern-variable-expression datum bindings)
  (if (eq '_ (extract-variable pattern-variable-expression))
      bindings
      (cons (make-binding
	     (extract-variable pattern-variable-expression)
	     datum)
	    bindings)))

(defun extract-variable (pattern-variable-expression)
  (second pattern-variable-expression))

(defun make-binding (variable datum)
  (list variable datum))

(defun find-binding (pattern-variable-expression binding)
  (unless (eq '_ (extract-variable pattern-variable-expression))
    (assoc (extract-variable pattern-variable-expression) binding)))

(defun extract-key (binding)
  (first binding))

(defun extract-value (binding)
  (second binding))

(defun match (p d &optional bindings)
  (cond
    ((and (atom p) (atom d))
     (format t "atom: ~a, ~a" p d)
     (if (eql p d)
	 bindings
	 'fail))
    ((and (listp p) (eq '? (first p)))
     (let ((binding (find-binding p bindings)))
       (if binding
	   (match (extract-value binding)
	     d
	     bindings)
	   (add-binding p d bindings))))

    ))
	 