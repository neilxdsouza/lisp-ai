
(defun stream-endp (stream)
  (eq stream 'empty-stream))

(defun stream-first (stream)
  (first stream))

(defun stream-rest (stream)
  (second stream))

(defun stream-cons (object stream)
  (list object stream))

(defun stream-append (stream1 stream2)
  (if (stream-endp stream1)
      stream2
      (stream-cons (stream-first stream1)
		   (stream-append (stream-rest stream1)
				  stream2))))

(defun stream-concatenate (streams)
  (format t "~a~%" streams)
  (if (stream-endp streams) 'empty-stream
      (if (stream-endp (stream-first streams))
	  (stream-concatenate (stream-rest streams))
	  (stream-cons (stream-first (stream-first streams))
		       (stream-concatenate
			(stream-cons (stream-rest (stream-first streams))
				     (stream-rest streams)))))))

(defun stream-transform (procedure stream)
  (if (stream-endp stream)
      'empty-stream
      (stream-cons (funcall procedure (stream-first stream))
		   (stream-transform procedure (stream-rest stream)))))

(defun stream-member (object stream)
  (cond ((stream-endp stream) nil)
	((equal object (stream-first stream)) t)
	(t (stream-member object (stream-rest stream)))))

(defmacro stream-remember (object variable)
  `(unless (stream-member ,object ,variable)
    (setf ,variable
	  (stream-append ,variable
			 (stream-cons ,object
				      'empty-stream)))
    ,object
    ))

(defmacro simple-macro (object variable)
  ;`(unless (stream-member ,object ,variable)
     ;(list ,object ,variable)
  ;   1
  ;   )
   `(unless (stream-member ,object ,variable)
      (list ,object ,variable)))
	 
	
			    
		       
      
(defmacro memq (obj lst)
  `(member ,obj ,lst :test #'eq))

(defmacro stream-cons (object stream)
  `(list ,object #'(lambda () ,stream)))

(defun stream-rest (stream)
  (funcall (second stream)))