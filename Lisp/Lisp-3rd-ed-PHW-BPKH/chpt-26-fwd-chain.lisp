    
;;; chpt 25 stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun stream-endp (stream)
  (eq stream 'empty-stream))

(defun stream-first (stream)
  (first stream))

(defun stream-rest (stream)
  (second stream))

(defun stream-cons (object stream)
  (list object stream))

(defun stream-append (stream1 stream2)
  (if (stream-endp stream1)
      stream2
      (stream-cons (stream-first stream1)
		   (stream-append (stream-rest stream1)
				  stream2))))

(defun stream-concatenate (streams)
  (format t "~a~%" streams)
  (if (stream-endp streams) 'empty-stream
      (if (stream-endp (stream-first streams))
	  (stream-concatenate (stream-rest streams))
	  (stream-cons (stream-first (stream-first streams))
		       (stream-concatenate
			(stream-cons (stream-rest (stream-first streams))
				     (stream-rest streams)))))))

(defun stream-transform (procedure stream)
  (if (stream-endp stream)
      'empty-stream
      (stream-cons (funcall procedure (stream-first stream))
		   (stream-transform procedure (stream-rest stream)))))

(defun stream-member (object stream)
  (cond ((stream-endp stream) nil)
	((equal object (stream-first stream)) t)
	(t (stream-member object (stream-rest stream)))))

(defmacro stream-remember (object variable)
  `(unless (stream-member ,object ,variable)
    (setf ,variable
	  (stream-append ,variable
			 (stream-cons ,object
				      'empty-stream)))
    ,object
    ))

(defmacro simple-macro (object variable)
  ;`(unless (stream-member ,object ,variable)
     ;(list ,object ,variable)
  ;   1
  ;   )
   `(unless (stream-member ,object ,variable)
      (list ,object ,variable)))
	 
	
			    
		       
      
(defmacro memq (obj lst)
  `(member ,obj ,lst :test #'eq))

;;;; chpt 25 
;;;;;;;;;;;;;;;;;;;;;;;

(defun remember-assertion (assertion)
  (stream-remember assertion *assertions*))

(setf *assertions* 'empty-stream)

(remember-assertion '(bozo is a dog))
(remember-assertion '(deedee is a horse))
(remember-assertion '(deedee is a parent of sugar))
(remember-assertion '(deedee is a parent of brassy))

(defun remember-rule (rule)
  (stream-remember rule *rules*))

(setf *rules* 'empty-stream)

(remember-rule
 '(identify
   ((? animal) is a (? species))
   ((? animal) is a parent of (? child))
   ((? child) is a (? species))))

(defun try-assertion (pattern assertion bindings)
  (let ((result (match pattern assertion bindings)))
    (if (eq 'fail result)
	'empty-stream
	(stream-cons result 'empty-stream))))

(try-assertion '((? animal) is a (? species))
			'(bozo is a dog)
			nil)

(try-assertion '((? animal) is a (? species))
			'(deedee is a horse)
			nil)


(defun match-pattern-to-assertions (pattern bindings)
  (stream-concatenate
   (stream-transform
    #'(lambda (assertion) (try-assertion pattern
					 assertion
					 bindings))
    *assertions*)))

(defun filter-binding-stream (pattern stream)
  (stream-concatenate
   (stream-transform
    #'(lambda (bindings)
	(match-pattern-to-assertions pattern bindings))
    stream)))

(defun apply-filters (patterns initial-input-stream)
  (if (endp patterns)
      initial-input-stream
      (apply-filters (rest patterns)
		     (filter-binding-stream (first patterns)
					    initial-input-stream))))

(defun instantiate-variables (pattern a-list)
  (cond
    ((atom pattern) pattern)
    ((eq '? (first pattern))
     (extract-value (find-binding pattern a-list)))
    (t (cons (instantiate-variables (first pattern) a-list)
	     (instantiate-variables (rest pattern) a-list)))))

(defun rule-name (rule)
  (first rule))

(defun rule-ifs (rule)
  (butlast (rest rule)))

(defun rule-then (rule)
  (first (last rule)))

(defun use-rule (rule)
  (let ((binding-stream
	 (apply-filters (rule-ifs rule)
			(stream-cons nil 'empty-stream))))
    (do ((binding-stream binding-stream
			 (stream-rest binding-stream))
	 (success-switch nil))
	((stream-endp binding-stream) success-switch)
      (let ((result (instantiate-variables
		     (rule-then rule)
		     (stream-first binding-stream))))
	(when (remember-assertion result)
	  (format t "~%Rule ~a indicates ~a."
		  (rule-name rule) result)
	  (setf success-switch t))))))
    
(defun forward-chain ()
  (do ((rule-stream *rules* (stream-rest rule-stream))
       (repeat-switch nil))
      ((stream-endp rule-stream)
       (if repeat-switch
	   (progn
	     (format t "~%I am trying the rules again")
	     (forward-chain))
	   (progn
	     (format t "~%Nothing new noted.")
	     'done)))
    (when (use-rule (stream-first rule-stream))
      (setf repeat-switch t))))

;;; test code - to see how the data looks like
;;; when stream-transform has operated on it
(let ((pattern '((? animal) is a (? species)))
	       (bindings 'nil))
	   (stream-transform #'(lambda (assertion)
				 (try-assertion pattern assertion bindings))
			     *assertions*))


;;;;;;;;;;;;;;;;;
;;; chpt 24 stuff
;;;;;;;;;;;;;;;;;;;;

(defun match (p d &optional bindings)
  (cond
    ((and (atom p) (atom d))
     ;; see if p and d are the same
     ;; if so return the value of bindings
     ;; otherwise fail
     (match-atoms p d bindings)
     )
    ((and (listp p) (eq '? (first p)))
     ;; see if the pattern variable is known
     ;; if it is, subst the value and try again
     ;; otherwise add new binding
     (match-variable p d bindings)
     )
    ((and (listp p) (listp d))
     ;; see if the first parts match producing new bindings
     ;; if they do not match, fail
     ;; if they do match, try the rest parts using the resulting bindings
     (match-pieces p d bindings)
     )
    (t 'fail)))

(defun match-atoms (p d bindings)
  (if (eql p d)
      bindings
      'fail))

(defun match-variable (p d bindings)
  (let ((binding (find-binding p bindings)))
    (if binding
	(match (extract-value binding) d bindings)
	(add-binding p d bindings))))

(defun match-pieces (p d bindings)
  (let ((result (match (first p) (first d) bindings)))
    (if (eq result 'fail)
	'fail
	(match (rest p) (rest d) result))))

(defun add-binding (pattern-variable-expression datum bindings)
  (if (eq '_ (extract-variable pattern-variable-expression))
      bindings
      (cons (make-binding
	     (extract-variable pattern-variable-expression)
	     datum)
	    bindings)))

(defun extract-variable (pattern-variable-expression)
  (second pattern-variable-expression))

(defun make-binding (variable datum)
  (list variable datum))



(defun find-binding (pattern-variable-expression binding)
  (unless (eq '_ (extract-variable pattern-variable-expression))
    (assoc (extract-variable pattern-variable-expression) binding)))

(defun extract-key (binding)
  (first binding))

(defun extract-value (binding)
  (second binding))

(setf *rules* 'empty-stream)
(remember-rule '(identify1
		 ((? animal) has hair)
		 ((? animal) is a mammal)))
(remember-rule '(identify2
		 ((? animal) gives milk)
		 ((? animal) is a mammal)))
(remember-rule '(identify3
		 ((? animal) has feathers)
		 ((? animal) is a bird)))
(remember-rule '(identify4
		 ((? animal) flies)
		 ((? animal) lays eggs)
		 ((? animal) is a bird)))
(remember-rule '(identify5
		 ((? animal) eats meat)
		 ((? animal) is a carnivore)))
(remember-rule '(identify6
		 ((? animal) has pointed teeth)
		 ((? animal) has claws)
		 ((? animal) has forward eyes)
		 ((? animal) is a carnivore)))
(remember-rule '(identify7
		 ((? animal) is a mammal)
		 ((? animal) has hoofs)
		 ((? animal) is an ungulate)))
(remember-rule '(identify8
		 ((? animal) is a mammal)
		 ((? animal) chews cud)
		 ((? animal) is an ungulate)))
(remember-rule '(identify9
		 ((? animal) is a mammal)
		 ((? animal) is a carnivore)
		 ((? animal) has tawny color)
		 ((? animal) has dark spots)
		 ((? animal) is a cheetah)))
(remember-rule '(identify10
		 ((? animal) is a mammal)
		 ((? animal) is a carnivore)
		 ((? animal) has tawny color)
		 ((? animal) has black stripes)
		 ((? animal) is a tiger)))
(remember-rule '(identify11
		 ((? animal) is an ungulate)
		 ((? animal) has long neck)
		 ((? animal) has long legs)
		 ((? animal) has dark spots)
		 ((? animal) is a giraffe)))
(remember-rule '(identify12
		 ((? animal) is an ungulate)
		 ((? animal) has black stripes)
		 ((? animal) is a zebra)))
(remember-rule '(identify13
		 ((? animal) is a bird)
		 ((? animal) does not fly)
		 ((? animal) has long neck)
		 ((? animal) has long legs)		 
		 ((? animal) is black and white)
		 ((? animal) is an ostrich )))	 	      
(remember-rule '(identify14
		 ((? animal) is a bird)
		 ((? animal) does not fly)
		 ((? animal) swims)
		 ((? animal) is black and white)
		 ((? animal) is a penguin)))
(remember-rule '(identify15
		 ((? animal) is a bird)
		 ((? animal) flies well)
		 ((? animal) is an albatross)))
(remember-rule '(identify16
		 ((? animal) is a (? species))
		 ((? animal) is a parent of (? child))
		 ((? child) is a (? species))))

(setf *assertions* 'empty-stream)
(remember-assertion '(robbie has dark spots))
(remember-assertion '(robbie has tawny color))
(remember-assertion '(robbie eats meat))
(remember-assertion '(robbie has hair))
(remember-assertion '(suzie has feathers))
(remember-assertion '(suzie flies well))

