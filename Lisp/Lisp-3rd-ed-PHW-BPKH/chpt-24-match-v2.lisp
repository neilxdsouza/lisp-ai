(defun match (p d &optional bindings)
  (cond
    ((and (atom p) (atom d))
     ;; see if p and d are the same
     ;; if so return the value of bindings
     ;; otherwise fail
     (match-atoms p d bindings)
     )
    ((and (listp p) (eq '? (first p)))
     ;; see if the pattern variable is known
     ;; if it is, subst the value and try again
     ;; otherwise add new binding
     (match-variable p d bindings)
     )
    ((and (listp p) (listp d))
     ;; see if the first parts match producing new bindings
     ;; if they do not match, fail
     ;; if they do match, try the rest parts using the resulting bindings
     (match-pieces p d bindings)
     )
    (t 'fail)))

(defun match-atoms (p d bindings)
  (if (eql p d)
      bindings
      'fail))

(defun match-variable (p d bindings)
  (let ((binding (find-binding p bindings)))
    (if binding
	(match (extract-value binding) d bindings)
	(add-binding p d bindings))))

(defun match-pieces (p d bindings)
  (let ((result (match (first p) (first d) bindings)))
    (if (eq result 'fail)
	'fail
	(match (rest p) (rest d) result))))

(defun add-binding (pattern-variable-expression datum bindings)
  (if (eq '_ (extract-variable pattern-variable-expression))
      bindings
      (cons (make-binding
	     (extract-variable pattern-variable-expression)
	     datum)
	    bindings)))

(defun extract-variable (pattern-variable-expression)
  (second pattern-variable-expression))

(defun make-binding (variable datum)
  (list variable datum))
    
	
     