(setf (get 's 'neighbours) '(a d)
      (get 'a 'neighbours) '(a b d)
      (get 'b 'neighbours) '(a c e)
      (get 'c 'neighbours) '(b)
      (get 'd 'neighbours) '(s a e)
      (get 'e 'neighbours) '(b d f)
      (get 'f 'neighbours) '(e))

(defun bugged-extend (path)
  (mapcar #' (lambda (new-node) (cons new-node path))
	     (get (first path) 'neighbours)))

(defun extend (path)
  (print (reverse path))
  (mapcar #' (lambda (new-node) (cons new-node path))
	     (remove-if #'(lambda (neighbour)
			    (member neighbour path))
			(get (first path) 'neighbours)))) 

(defun dfs (start finish &optional (queue
				    (list (list start))))
  (print queue)
  (cond ((endp queue) nil)
	((eq finish (first (first queue)))
	 (reverse (first queue)))
	(t (dfs start finish (append (extend (first queue))
				     (rest queue))))))

(defun bfs (start finish &optional (queue
				    (list (list start))))
  (cond ((endp queue)
	 nil)
	((eq finish (first (first queue)))
	 (reverse (first queue)))
	(t (bfs start finish (append (rest queue)
				     (extend (first queue)))))))

(defun dfs-noq (start finish &optional (current-path
					(list start)))
  (print current-path)
  (cond ((endp current-path) nil)
	((eq finish (first current-path))
	 (reverse current-path))
	(t (dfs-noq start finish (append (extend-noq current-path)
					 current-path)))))

(defun extend-noq (path)
  (print (reverse path))
  ;(mapcar #' (lambda (new-node) (cons new-node path))
  ;	     (remove-if #'(lambda (neighbour)
  ;			    (member neighbour path))
  ;			(get (first path) 'neighbours))))
  (append (remove-if #'(lambda (neighbour)
			 (member neighbour path))
		     (get (first path) 'neighbours))
	  path))

	   
;;; 23-oct-2011

(defun best-first (start finish &optional (queue (list (list start))))
  (cond ((endp queue) nil)
	((eq finish (first
		     (first queue)))
	 (reverse (first queue)))
	(t (best-first start finish (sort (append (extend (first queue))
						  (rest queue))
					  #'(lambda (p1 p2)
					      (closerp p1 p2 finish)))))))
(defun straight-line-distance (p1 p2)
  (let ((coord1 (get p1 'coordinates))
	(coord2 (get p2 'coordinates)))
    (sqrt (+ (expt (- (first coord1) (first coord2))
		   2)
	     (expt (- (second coord1) (second coord2))
		   2)))))
	
	  

(defun closerp (path1 path2 finish)
   (< (straight-line-distance (first path1) finish)
      (straight-line-distance (first path2) finish)))

;;; data for best first test
(setf (get 's 'coordinates) '(0 3)
      (get 'a 'coordinates) '(4 6)
      (get 'b 'coordinates) '(7 6)
      (get 'c 'coordinates) '(11 6)
      (get 'd 'coordinates) '(3 0)
      (get 'e 'coordinates) '(6 0)
      (get 'f 'coordinates) '(11 3))
  

(defun best-first-improved-prob-19-3 (start finish &optional (queue (list (list start))))
  (cond ((endp queue) nil)
	((eq finish (first
		     (first queue)))
	 (reverse (first queue)))
	(t (best-first start finish
		       ;(sort (append (extend (first queue))
			;			  (rest queue))
			;		  #'(lambda (p1 p2)
			;		      (closerp p1 p2 finish)))))))
		       (merge
			'list 
			(sort (extend (first queue))
				    #'(lambda (p1 p2)
					(closerp p1 p2 finish)))
			(rest queue)
			#'(lambda (p1 p2)
			    (closerp p1 p2 finish))
			)))))

;; needs atleast 2 nodes in nodes - else will fail badly
;; '1 2 => dist (1,2)
;; '1 2 3 => dist (1,2) '(2,3) => dist(1,2) + dist(2,3)
;; '1 2 3 4 => dist (1,2) '(2 3 4) => dist(1,2) + dist(2,3) '(3 4)
;;             => dist(1,2) + dist (2,3) + dist(3,4) '4
(defun path-length (nodes &optional (current-length 0))
  (print nodes)
  (print current-length)
  (if (or
        (null nodes)
	(null (rest nodes)))
      current-length
      (path-length (rest nodes)
		   (+ current-length
		      (straight-line-distance (first nodes) (first (rest nodes)))))
     ))
  