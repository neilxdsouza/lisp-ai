;library of functions used in PAIP
;could be useful elsewhere too
;

(defun mappend (fn the-list)
  "apply fn element to each element of the list and append the result"
  (apply #'append (mapcar fn the-list)))

(defun self-and-double (x)
  (list x (+ x x)))