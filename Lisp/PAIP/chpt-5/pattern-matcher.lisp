(defun variable-p (x)
  "is x a logical (prolog like) variable"
  (and (symbolp x) (equal (char (symbol-name x) 0) #\?)))

;; sublis example - used to do the substitution
(sublis '((?x . vacation))
		 '(what would it mean to you if you got a ?x ?))

(defconstant fail nil
  "indicates pat match failure")

(defconstant no-bindings '((t . t))
  "indicates pat-match success with no bindings")

(defun get-binding (var bindings)
  "find a (variable . value) pair in a binding list"
  (assoc var bindings))

(defun binding-val (binding)
  "get the value part of a single binding"
  (cdr binding))

(defun lookup (var bindings)
  "get the value part (for var) from the bindings list"
  (binding-val (get-binding var bindings)))

(defun extend-bindings (var val bindings)
  (cons (cons var val) bindings))

(defun pat-match (pattern input &optional (bindings no-bindings))
  "match pattern against input in the context of the bindings"
  (format t "pat-match    pattern: ~a input:~a bindings:~a ~%"
	  pattern input bindings)
  (cond ((eq bindings fail) fail)
	((variable-p pattern)
	 (format t "took path match-variable")
	 (match-variable pattern input bindings))
	((eql pattern input) bindings)
	((segment-pattern-p pattern)
	 (segment-match pattern input bindings))
	((and (consp pattern) (consp input))
	 (pat-match (rest pattern) (rest input)
		    (pat-match (first pattern) (first input)
			       bindings)))
	(t fail)))

(defun match-variable (var input bindings)
  (format t "match-variable    pattern: ~a input:~a bindings:~a ~%"
	  var input bindings)
  (let ((binding (get-binding var bindings)))
    (cond ((not binding) (extend-bindings var input bindings))
	  ((equal input (binding-val binding)) bindings)
	  (t fail))))

(defun segment-pattern-p (pattern)
  (and (consp pattern)
       (starts-with (first pattern) '?*)))

(defun segment-match (pattern input bindings &optional (start 0))
  "match the segment pattern ((?* var) . pat) against input."
  (format t "segment-match pattern: ~a input:~a bindings:~a ~%"
	  pattern input bindings)
  (let ((var (second (first pattern)))
	(pat (rest pattern)))
    (format t "segment-match var: ~a, pat: ~a~%"
	    var pat)
    (if (null pat)
	(match-variable var input bindings)
	(let ((pos (position (first pat) input
			     :start start :test #'equal)))
	  (format t "seaching position ~a in input ~a, pos: ~a~%"
		  (first pat) input pos)
	  (if (null pos)
	      fail
	      (let ((b2 (pat-match
			 pat (subseq input pos)
			 (match-variable var (subseq input 0 pos)
			 		 bindings)
			 ;bindings
			 )))
		(if (eq b2 fail)
		    (progn
		      (format t "pat-match with inputs: pattern ~a inputs ~a bindings ~a FAILED~%"
			      pat (subseq input pos) (match-variable var (subseq input 0 pos) bindings))
		      (segment-match pattern input bindings (+ pos 1)))
		    ;(match-variable var (subseq input 0 pos) b2)
		    (progn
		      (format t "b2 did not FAIL: it is ~a~%"
			      b2
			      )
		      b2)
		    )))))))
		
	

(defun starts-with (list x)
  (and (consp list) (eql (first list) x)))