(defun executing-p (x)
  "is x of the form: (executing ...) ?"
  (starts-with x 'executing))

(defun starts-with (list x)
  "is this a list whose first element is x?"
  (and (consp list)
       (eql (first list) x)))

(defun convert-op (op)
  "make op conform to the (executing op) convention"
  (unless (some #'executing-p (op-add-list op))
    (push (list 'executing (op-action op)) (op-add-list op)))
  op)

(defparameter *school-ops*
  (list
   (make-op :action 'drive-son-to-school
	    :preconds '(son-at-home car-works)
	    :add-list '(son-at-school)
	    :del-list '(son-at-home)
	    )
   (make-op :action 'shop-installs-battery
	    :preconds '(car-needs-battery shop-knows-problem shop-has-money)
	    :add-list '(car-works)
	    )
   (make-op :action 'tell-shop-problem
	    :preconds '(in-communication-with-shop)
	    :add-list '(shop-knows-problem)
	    )
   (make-op :action 'telephone-shop
	    :preconds '(know-phone-number)
	    :add-list '(in-communication-with-shop)
	    )
   (make-op :action 'look-up-number
	    :preconds '(have-phone-book)
	    :add-list '(know-phone-number)
	    )
   (make-op :action 'give-shop-money
	    :preconds '(have-money)
	    :add-list '(shop-has-money)
	    :del-list '(have-money)
	    )))


(defun op (action &key preconds add-list del-list)
  "make a new operator that obeys the (EXECUTING op) convention"
  (convert-op
   (make-op :action action
	    :preconds preconds
	    :add-list add-list
	    :del-list del-list)))
    

(defun achieve-all (goals)
  (and (every #'achieve goals)
       (subsetp goals *state*)))

(defvar *dbg-ids* nil "identifiers used by dbg")

(defun dbg (id format-string &rest args)
  "Print debugging info if (DEBUG ID) has been specified"
  (when (member id *dbg-ids*)
    (fresh-line *debug-io*)
    (apply #'format *debug-io* format-string args)))

(defun debug (&rest ids)
  "start dbg output on given ids"
  (setf *dbg-ids* (union ids *dbg-ids*)))

(defun undebug (&rest ids)
  "stop dbg on the ids. with no ids stop debug altogether"
  (setf *dbg-ids* (if (null ids) nil
		      (set-difference *dbg-ids* ids))))

(defun dbg-indent (id indent format-string &rest args)
  "print intended dbging info if (DEBUG ID) has been specified"
  (when (member id *dbg-ids*)
    (fresh-line *debug-io*)
    (dotimes (i indent)
      (princ " " *debug-io*))
    (apply #'format *debug-io* format-string args)))

(defun achieve-all (state goals goal-stack)
  (let ((current-state state))
    (if (and (every #'(lambda (g)
			     (setf current-state
				   (achieve current-state g goal-stack)))
		    goals)
	     (subsetp goals current-state :test #'equal))
	current-state)))

(defun achieve (state goal goal-stack)
  "a goal is achieved if it already holds.
   or if there is an appropriate op for it that is applicable"
  (dbg-indent :gps (length goal-stack) "Goal: ~a" goal)
  (cond ((member-equal goal state) state)
	((member-equal goal goal-stack) nil)
	(t (some #'(lambda (op) (apply-op state goal op goal-stack))
		 (find-all goal *ops* :test #'appropriate-p)))))

(defun apply-op (state goal op goal-stack)
  "return a new transformed state if op is applicable"
  (dbg-indent :gps (length goal-stack) "Consider: ~a" (op-action op))

  (let ((state2 (achieve-all state (op-preconds op)
			     (cons goal goal-stack))))
    (unless (null state2)
      ;; return an updated state
      (dbg-indent :gps (length goal-stack) "Action: ~a" (op-action op))
      (append (remove-if #'(lambda (x)
			     (member-equal x (op-del-list op)))
			 state2)
	      (op-add-list op)))))

(defun appropriate-p (goal op)
  (member-equal goal (op-add-list op)))

(defun member-equal (item list)
  (member item list :test #'equal))

(defun use (oplist)
  "use oplist as the default list of operators"
  (length (setf *ops* oplist)))

;; no (*ops* *ops*) is not a mistake
;; refer page 130 PAIP

(defun GPSv2_1 (state goals &optional (*ops* *ops*))
  "General problem solver: from state, achieve goals using *ops*"
  (remove-if #'atom (achieve-all (cons '(start) state) goals nil)))

(defun GPS (state goals &optional (ops *ops*))
  "General problem solver: from state, achieve goals using *ops*"
  (let ((old-ops *ops*))
    (setf *ops* ops)
    (let ((result (remove-if #'atom
			     (achieve-all (cons '(start) state)
					  goals nil))))
      (setf *ops* old-ops)
      result)))

(defparameter *banana-ops*
  (list
   (op 'climb-on-chair
       :preconds '(chair-at-middle-room at-middle-room on-floor)
       :add-list '(at-bananas on-chair)
       :del-list '(at-middle-room on-floor))
   (op 'push-chair-from-door-to-middle-room
       :preconds '(chair-at-door at-door)
       :add-list '(chair-at-middle-room at-middle-room)
       :del-list '(chair-at-door at-door))
   (op 'walk-from-door-to-middle-room
       :preconds '(at-door on-floor)
       :add-list '(at-middle-room)
       :del-list '(at-door))
   (op 'grasp-bananas
       :preconds '(at-bananas empty-handed)
       :add-list '(has-bananas)
       :del-list '(empty-handed))
   (op 'drop-ball
       :preconds '(has-ball)
       :add-list '(empty-handed)
       :del-list '(has-ball))
   (op 'eat-bananas
       :preconds '(has-bananas)
       :add-list '(empty-handed not-hungry)
       :del-list '(has-bananas hungry))))


;;;;;; maze problem - gps

(defun make-maze-ops (pair)
  "make maze ops in both directions"
  (list (make-maze-op (first pair) (second pair))
	(make-maze-op (second pair) (first pair))))

(defun make-maze-op (here there)
  "make an operator to move between 2 places"
  (op `(move from ,here to ,there)
      :preconds `((at ,here))
      :add-list `((at ,there))
      :del-list `((at ,here))))

(defparameter *maze-ops*
  (mappend #'make-maze-ops
	   `((1 2) (2 3) (3 4) (4 9) (9 14) (9 8) (8 7) (7 12) (12 13)
	     (12 11) (11 6) (11 16) (16 17) (17 22) (22 21) (22 23)
	     (23 18) (23 24) (24 19) (19 20) (20 15) (15 10) (10 5) (20 25))))
	     
