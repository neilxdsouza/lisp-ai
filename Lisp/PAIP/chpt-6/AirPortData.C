
#include "AirPortData.h"

map<string, AirPortData *> airport_data_map;



AirPortData::AirPortData (
	string p_icao_code, string p_iata_code, string p_airport_name, 
	string p_city_name, string p_country_name, 
	string p_lat_deg, string p_lat_min, string p_lat_sec, string p_lat_ori, 
	string p_lon_deg, string p_lon_min, string p_lon_sec, string p_lon_ori
	):
	icao_code (p_icao_code), iata_code (p_iata_code), airport_name (p_airport_name), 
	city_name (p_city_name), country_name (p_country_name), 
	lat_deg (p_lat_deg), lat_min (p_lat_min), lat_sec (p_lat_sec), lat_ori (p_lat_ori), 
	lon_deg (p_lon_deg), lon_min (p_lon_min), lon_sec (p_lon_sec), lon_ori (p_lon_ori)
{ }

