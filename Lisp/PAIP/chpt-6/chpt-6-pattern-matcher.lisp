(defconstant fail nil "indicates pat-match failure")

(defconstant no-bindings '((t . t))
  "indicates pat-match success with no variables")

(defun pat-match (pattern input &optional (bindings no-bindings))
  (format t "pat-match: pattern ~a, input ~a, bindings ~a~%"
	  pattern input bindings)
  (cond ((eq bindings fail)
	 (format t "too the eq bindings fail condn ~%")
	 fail)
	((variable-p pattern)
	 (match-variable pattern input bindings))
	((eql pattern input)
	 bindings)
	((segment-pattern-p pattern)
	 (segment-matcher pattern input bindings))
	((single-pattern-p pattern)
	 (single-matcher pattern input bindings))
	((and (consp pattern) (consp input))
	 (pat-match (rest pattern) (rest input)
		    (pat-match (first pattern) (first input)
			       bindings)))
	(t
	 (format t "reached the failure catch all condition")
	 fail)))

(defun variable-p (x)
  "is x a logical (prolog like) variable"
  (and (symbolp x) (equal (char (symbol-name x) 0) #\?)))

(defun get-binding (var bindings)
  "find a (variable . value) pair in a binding list"
  (assoc var bindings))

(defun binding-val (binding)
  "get the value part of a single binding"
  (cdr binding))

(defun binding-var (binding)
  "get the value part of a single binding"
  (car binding))

(defun lookup (var bindings)
  "get the value part (for var) from the bindings list"
  (binding-val (get-binding var bindings)))

(defun extend-bindings (var val bindings)
  (cons (make-binding var val)
	(if (eq bindings no-bindings)
	    nil
	    bindings)))
	;bindings))


(defun make-binding (var val)
  (cons var val))

(defun match-variable (var input bindings)
  (let ((binding (get-binding var bindings)))
    (cond ((not binding)
	   (extend-bindings var input bindings))
	  ((equal input (binding-val binding)) bindings)
	  (t fail))))

(setf (get '?is 'single-match) 'match-is)
(setf (get '?or 'single-match) 'match-or)
(setf (get '?and 'single-match) 'match-and)
(setf (get '?not 'single-match) 'match-not)

(setf (get '?* 'segment-match) 'segment-match)
(setf (get '?+ 'segment-match) 'segment-match+)
(setf (get '?? 'segment-match) 'segment-match?)
(setf (get '?if 'segment-match) 'match-if)

(defun segment-pattern-p (pattern)
  (and (consp pattern) (consp (first pattern))
       (symbolp (first (first pattern)))
       (segment-match-fn (first (first pattern)))))

(defun single-pattern-p (pattern)
  (and (consp pattern)
       (single-match-fn (first pattern))))

(defun segment-matcher (pattern input bindings)
  (funcall (segment-match-fn (first (first pattern)))
	   pattern input bindings))

(defun single-matcher (pattern input bindings)
  (funcall (single-match-fn (first pattern))
	   (rest pattern) input bindings))

(defun segment-match-fn (x)
  (when (symbolp x)
    (get x 'segment-match)))

(defun single-match-fn (x)
  (when (symbolp x)
    (get x 'single-match)))

(defun match-is (var-and-pred input bindings)
  (let* ((var (first var-and-pred))
	 (pred (second var-and-pred))
	 (new-bindings (pat-match var input bindings)))
    (if (or (eq new-bindings fail)
	    (not (funcall pred input)))
	fail
	new-bindings)))

(defun match-and (patterns input bindings)
  (cond ((eq bindings fail) fail)
	((null patterns) bindings)
	(t (match-and (rest patterns)
		      input
		      (pat-match (first patterns)
				 input
				 bindings)))))

(defun match-or (patterns input bindings)
  (if (null patterns)
      fail
      (let ((new-bindings (pat-match (first patterns)
				     input bindings)))
	(if (eq new-bindings fail)
	    (match-or (rest patterns) input bindings)
	    new-bindings))))

(defun match-not (patterns input bindings)
  (if (match-or patterns input bindings)
      fail
      bindings))

(defun segment-match (pattern input bindings &optional (start 0))
  (let ((var (second (first pattern)))
	(pat (rest pattern)))
    (format t "segment-match pattern: ~a input: ~a bindings: ~a~%var: ~a pat:~a (null pat)~a~%"
	    pattern input bindings var pat (null pat))
    (if (null pat)
	(match-variable var input bindings)
	(let ((pos (first-match-pos (first pat) input start)))
	  (format t "pat: ~a, (first pat): ~a, pos: ~a~%"
		  pat (first pat) pos)
	  (if (null pos)
	      fail
	      (let ((b2 (pat-match
			 pat (subseq input pos)
			 (match-variable var (subseq input 0 pos)
					 bindings))))
		(format t "b2 = pat-match invoked with input: ~a~%"
			(subseq input pos))
		(format t "calls to match-variable params : var ~a input ~a bindings ~a~%"
			var (subseq input 0 pos) bindings)
		(if (eq b2 fail)
		    (segment-match pattern input bindings (+ pos 1))
		    b2)))))))

(defun first-match-pos (pat1 input start)
  (cond ((and (atom pat1) (not (variable-p pat1)))
	 (position pat1 input :start start :test #'equal))
	((< start (length input)) start)
	(t nil)))

;; something for diff

(defun segment-match+ (pattern input bindings)
  "Match one or more elements of input"
  (segment-match pattern input bindings 1))

(defun segment-match? (pattern input bindings)
  (let ((var (second (first pattern)))
	(pat (rest pattern)))
    (or (pat-match (cons var pat) input bindings)
	(pat-match pat input bindings))))

(defun match-if (pattern input bindings)
  "Test an arbitrary expression involving variables
   The pattern looks like ((?if code)) . rest)."
  (format t "match-if was invoked with pattern ~a, input ~a, bindings ~a~%"
	  pattern input bindings)
  (format t "call to eval looks like this: ~a~%"
	  (second (first pattern)))
  (format t "mapcar car bindings: ~a~%"
	  (mapcar #'car bindings))
  (format t "mapcar cdr bindings: ~a~%"
	  (mapcar #'cdr bindings))
  (and (progv
	   (mapcar #'car bindings)
	   (mapcar #'cdr bindings)	 
	 (eval (second (first pattern))))
       (pat-match (rest pattern) input bindings)))
  
	   
	   
  