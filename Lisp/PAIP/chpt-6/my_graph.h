/*
 * =====================================================================================
 *
 *       Filename:  my_graph.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 13 August 2012 06:41:42  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include <string>
#include <map>
#include <set>

using std::string;
using std::map;
using std::set;

struct MyCityGraphNode;

struct MyFlightEdge
{
	MyCityGraphNode * origin_, * destination_;
	string eff_start_date;
	string eff_end_date;
	string flight_no;
	MyFlightEdge (string p_org, string p_dest,
			string p_eff_start_date, string p_eff_end_date,
			string p_flight_no);
};

struct MyCityGraphNode
{
	string cityName_;
	int latitude_, longitude_;
	int nFlightsPassThroughHere_;
	set <string> flight_nos_set;
	MyCityGraphNode (string city_name, int lat = 0, int lon = 0);
	int get_flights_pass_through();
};

struct AirCarrier
{
	string carrierName_;
	AirCarrier (string p_carrier_name);
};

extern map <string, MyCityGraphNode*> city_nodes_map;
extern map <string, AirCarrier*> air_carriers_map;
extern map <string, MyFlightEdge*> flight_edges_origin;
extern map <string, MyFlightEdge*> flight_edges_dest;

