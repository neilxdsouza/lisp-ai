/*
 * =====================================================================================
 *
 *       Filename:  AiportData.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  Monday 13 August 2012 10:04:53  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include <map>
#include <string>

using std::map;
using std::string;

struct AirPortData
{
	string icao_code, iata_code, airport_name,
	       city_name, country_name, 
	       lat_deg, lat_min, lat_sec, lat_ori,
	       lon_deg, lon_min, lon_sec, lon_ori;

	AirPortData (
		string p_icao_code, string p_iata_code, string p_airport_name, 
		string p_city_name, string p_country_name, 
		string p_lat_deg, string p_lat_min, string p_lat_sec, string p_lat_ori, 
		string p_lon_deg, string p_lon_min, string p_lon_sec, string p_lon_ori
	       );

	double to_lat_deg_min();
	double to_lon_deg_min();

};

extern map<string, AirPortData *> airport_data_map;
