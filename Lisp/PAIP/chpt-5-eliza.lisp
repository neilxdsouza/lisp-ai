(defun simple-equal (x y)
  (if (or (atom x) (atom y))
      (eql x y)
      (and (simple-equal (first x) (first y))
	   (simple-equal (rest x) (rest y)))))

(defun pat-match (pattern input)
  (if (variable-p pattern)
      t
      (if (or (atom pattern) (atom input))
	  (eql pattern input)
	  (and (pat-match (first pattern) (first input))
	       (pat-match (rest pattern) (rest input))))))

(defun variable-p (x)
  (and (symbolp x) (equal (char (symbol-name x) 0) #\?)))


			 
;	  (let ((var1 'neil)
;	       (var2 'atul))
;	   (print (symbol-name var1))
;	   (print (symbol-name var2)))
	   
	   

;"NEIL" 
;"ATUL" 
;"ATUL"
;CL-USER> (char "neil" 0)
;#\n
;CL-USER> (char "neil" 1)
; compiling (DEFUN PAT-MATCH ...)#\e
;CL-USER> (pat-match '(I need a ?X) '(I need a vacation))
;
;T
;CL-USER> (pat-match '(I need a ?X) '(I really need a vacation))
;
;NIL

(defun pat-match-attempt-1 (pattern input)
  "buggy"
  (print pattern)
  (print input)
  (if (variable-p pattern)
      (list (cons pattern input))
      (if (or (atom pattern) (atom input))
	  (eql pattern input)
	  (append (pat-match-attempt-1 (first pattern) (first input))
		  (pat-match-attempt-1 (rest pattern) (rest input)))))))

(defconstant fail nil)
; no need for below: already defined it seems
;(defconstant no-bindings '( (t . t)))

(defun get-binding (var bindings)
  (assoc var bindings))

(defun binding-val (binding)
  (cdr binding))

(defun lookup (var bindings)
  (binding-val (get-binding var bindings)))

(defun extend-bindings (var val bindings)
  (cons (cons var val) bindings))

(defun pat-match-v2 (pattern input &optional (bindings no-bindings))
					;(progn
					;(print 'pattern pattern)
					;(print 'input input)
					;(print 'bindings bindings)
  (print (list 'pattern pattern))
  (print (list 'input input))
  (print (list 'bindings bindings))
  
  (cond ((eq bindings fail)
	 (print '(empty bindings))
	 fail)
	((variable-p pattern)
	 (match-variable pattern input bindings))
	((eql pattern input)
	 (progn
	   (print (list 'eql pattern input))
	   bindings))
	((and (consp pattern) (consp input))
	 (progn
	   (print (list 'breaking-it-down (rest pattern) (rest input)))
	   (pat-match-v2 (rest pattern) (rest input)
			 (pat-match-v2 (first pattern) (first input) bindings))))
	(t fail)))
  
(defun match-variable (var input bindings)
  (let ((binding (get-binding var bindings)))
    (cond ((not binding) (extend-bindings var input bindings))
	  ((equal input (binding-val binding)) bindings)
	  (t fail))))

;CL-USER> (pat-match-v2 '(i need a ?x) '(i need a vacation))

	


	 