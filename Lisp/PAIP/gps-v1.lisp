(defvar *state* nil "The current state:  a list ofo conditions")

(defvar *ops* nil "A list of available operators")

(defstruct op "An operation"
	   (action nil)
	   (preconds nil)
	   (add-list nil)
	   (del-list nil)
	   )

(defun GPS (*state* goals *ops*)
  "General Problem Solver: achieve all goals using *ops*"
  (if (every #'achieve goals) 'solved))

(defun achieve (goal)
  (print (list 'achieve goal 'state *state*))
  (or (member goal *state*)
      (progn
	(print (list 'trying-to-achieve-goal goal))
	(some #'apply-op
	      (find-all goal *ops* :test #'appropriate-p)))))

(defun find-all (item sequence &rest keyword-args
		 &key (test #'eql) test-not &allow-other-keys)
  (if test-not
      (apply #'remove item sequence
	     :test-not (complement test-not) keyword-args)
      (apply #'remove item sequence
	     :test (complement test) keyword-args)))

(defun appropriate-p (goal op)
  (member goal (op-add-list op)))

(defun apply-op (op)
  (print (list 'apply-op op))
  (print (list 'op-preconds (op-preconds op)))
  (when (every #'achieve (op-preconds op))
    (print (list 'executing (op-action op)  'state *state*))
    (setf *state* (set-difference *state* (op-del-list op)))
    (setf *state* (union *state* (op-add-list op)))
    t))

;;; ========================

(defparameter *school-ops*
  (list
   (make-op :action 'drive-son-to-school
	    :preconds '(son-at-home car-works)
	    :add-list '(son-at-school)
	    :del-list '(son-at-home))
   (make-op :action 'shop-installs-battery
	    :preconds '(car-needs-battery shop-knows-problem shop-has-money)
	    :add-list '(car-works))
   (make-op :action 'tell-shop-problem
	    :preconds '(in-communication-with-shop)
	    :add-list '(shop-knows-problem))
   (make-op :action 'telephone-shop
	    :preconds '(know-phone-number)
	    :add-list '(in-communication-with-shop))
   (make-op :action 'look-up-number
	    :preconds '(have-phone-book)
	    :add-list '(know-phone-number))
   (make-op :action 'give-shop-money
	    :preconds '(have-money)
	    :add-list '(shop-has-money)
	    :del-list '(have-money))))

;;; sample input

(gps '(son-at-home car-needs-battery have-money have-phone-book)
	      '(son-at-school) *school-ops*)
	    
	    
	    
	

